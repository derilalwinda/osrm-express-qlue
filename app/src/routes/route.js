const express = require("express");
const logfmt = require("logfmt");

const router = express.Router();

router.post("/", (req, res) => {
  if (!req.body.coordinates) {
    return res.status(422).json({ error: "Missing coordinates" });
  }

  const osrm = req.app.get("osrm");
  const options = {
    coordinates: req.body.coordinates,
    alternatives: req.body.alternatives || false,
    // Return route steps for each route leg
    steps: req.body.steps || false,
    // Return annotations for each route leg
    annotations: req.body.annotations || false,
    // Returned route geometry format. Can also be geojson
    geometries: req.body.geometries || "polyline",
    // Add overview geometry either full, simplified according to
    // highest zoom level it could be display on, or not at all
    overview: req.body.overview || "false",
    // Forces the route to keep going straight at waypoints and don't do
    // a uturn even if it would be faster. Default value depends on the profile
    continue_straight: req.body.continue_straight || false
  };

  try {
    osrm.route(options, (err, result) => {
      if (err) {
        return res.status(422).json({ error: err.message });
      }
      return res.json(result);
    });
  } catch (err) {
    logfmt.error(new Error(err.message));
    return res.status(500).json({ error: err.message });
  }
});



router.get("/v1/driving/:coordinates", (req, res) => {
  if (!req.params.coordinates) {
    return res.status(422).json({ error: "Missing coordinates" });
  }
  var stringCoordinates = req.params.coordinates;
  var arrayCoordinates = stringCoordinates.split(";");
  var alternatives;
  var steps;
  var annotations;
  var continue_straight;

  arrayCoordinates.forEach(function(item,index){
  	var arrayItem = item.split(",");
    arrayCoordinates[index] = new Array(
    arrayItem[0] * 1 ,
    arrayItem[1] * 1)
  })
  const osrm = req.app.get("osrm");
  if(req.query.alternatives == "true"){
    alternatives = true;
  }else{
    alternatives = false;
  }
  if(req.query.steps == "true"){
    steps = true;
  }else{
    steps = false;
  }
  if(req.query.annotations == "true"){
    annotations = true;
  }else{
    annotations = false;
  }
  if(req.query.continue_straight == "true"){
    continue_straight = true;
  }else{
    continue_straight = false;
  }
  const options = {
    coordinates: arrayCoordinates,
    alternatives: alternatives || false,
    // Return route steps for each route leg
    steps: steps || false,
    // Return annotations for each route leg
    annotations: annotations || false,
    // Returned route geometry format. Can also be geojson
    geometries: req.query.geometries || "polyline",
    // Add overview geometry either full, simplified according to
    // highest zoom level it could be display on, or not at all
    overview: req.query.overview || "false",
    // Forces the route to keep going straight at waypoints and don't do
    // a uturn even if it would be faster. Default value depends on the profile
    continue_straight: continue_straight || false
  };

  try {
    osrm.route(options, (err, result) => {
      if (err) {
        return res.status(422).json({ error: err.message });
      }
      result["code"] = "Ok";
      return res.json(result);
    });
  } catch (err) {
    logfmt.error(new Error(err.message));
    return res.status(500).json({ error: err.message });
  }
});

module.exports = router;
